#! /bin/sh

set -ex

guix pull -p ~/.config/guix/deploy --channels=channels.scm
. ~/.config/guix/deploy/etc/profile

guix describe > .guix-describe

if ! test -f id_rsa; then
    ssh-keygen -f id_rsa -P ''
fi

image=$(guix system vm-image --image-size=2G bare-bones.scm)
#image=$(~/src/guix/version-0.16.0/pre-inst-env guix system vm-image --image-size=2G bare-bones.scm)
echo vm.sh:bare-bones:$image
cp $image .
copy=$(basename $image)
chmod +w $copy
guix environment --ad-hoc qemu -- qemu-system-x86_64 -enable-kvm -m 1G -net nic -net user,hostfwd=tcp:127.0.0.1:10022-:22 -device virtio-blk,drive=guix -drive if=none,id=guix,file=$copy&

ssh-keygen -R [localhost]:10022
while ! ssh -o StrictHostKeyChecking=no -i id_rsa -p 10022 root@localhost ls; do
    sleep 1;
done

GUILE_LOAD_PATH=.:$GUILE_LOAD_PATH guix deploy deploy-vm.scm
ssh -i id_rsa -p 10022 root@localhost guix system list-generations
