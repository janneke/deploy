(use-modules (gnu))
(use-modules (bare-bones))
(use-service-modules shepherd)

(define %system
  (operating-system
    (inherit %bare-bones)
    (host-name "deployable")

    (packages (cons* hello %base-packages))

    (services
     (cons* (service (shepherd-service-type
                      'hello
                      (lambda _
                        (shepherd-service
                         (provision '(hello))
                         (start '(lambda _ (display "hello\n")))
                         (stop '(lambda _ (display "goodbye\n"))))))
                     #t)
            (operating-system-user-services %bare-bones)))))

(list (machine
       (system %system)
       (environment managed-host-environment-type)
       (configuration (machine-ssh-configuration
                       (host-name "localhost")
                       (identity "id_rsa")
                       (port 10022)))))
