(define-module (bare-bones)
  #:use-module (ice-9 rdelim)
  #:use-module (gnu)
  #:use-module (gnu services networking)
  #:use-module (gnu services ssh)
  #:export (%bare-bones))

(define %bare-bones
  (operating-system
    (host-name "baar")
    (timezone "Europe/Amsterdam")
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (target "/dev/vda")))
    (file-systems (cons (file-system
                          ;; after reboot: no such device: guix
                          ;; (device (file-system-label "guix"))
                          (device "/dev/vda1")
                          (mount-point "/")
                          (type "ext4"))
                        %base-file-systems))
    (services
     (cons*
      (service dhcp-client-service-type)
      (service openssh-service-type
               (openssh-configuration
                (permit-root-login #t)
                (authorized-keys
                 `(("root" ,(local-file "id_rsa.pub"))))
                (port-number 22)))
      (modify-services %base-services
        (guix-service-type
         config => (guix-configuration
                    (inherit config)
                    (authorized-keys
                     (cons
                      (local-file "/etc/guix/signing-key.pub")
                      %default-authorized-guix-keys)))))))))

%bare-bones
